import multiprocessing as mp
import numpy as np
import cPickle as pk
import collections


def check_overlap(dataset1, start, end, dataset2, q):
    ''' compute the number of data in dataset1 existing in dataset2[start to end] '''
    overlappingindex = []
    length1 = dataset1.shape[0]
    for i in xrange(length1):
        for j in xrange(start, end):
            error = np.sum(np.sum(np.abs(dataset1[i,:,:]-dataset2[j,:,:])))
            if error < 0.2:
                overlappingindex.append((i,j))
                break
    q.put(tuple(overlappingindex))
    return None



print('start loading notMNIST.pickle')
with open('notMNIST.pickle', 'rb') as f:
    data = pk.load(f)
print('loading finished.')
q = mp.Queue()
train_dataset = data['train_dataset']
valid_dataset = data['valid_dataset']
length = train_dataset.shape[0]
dx = (0, int(length*1.0/3), int(length*2.0/3), length)
processes = []
for i in xrange(3):
    processes.append(mp.Process(target=check_overlap, args=(valid_dataset, idx[i], idx[i+1], train_dataset, q)))
for p in processes:
    p.start()
for p in processes:
    p.join()
results = []
while not q.empty():
    results.append(q.get())
print(results)
func  = lambda x: [i  for xs in x for i in func(xs)] if isinstance(x,collections.Iterable) else [x]
print(func(result))
