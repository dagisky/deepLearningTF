import tensorflow as tf
import numpy as np

class TextCNN(object):
	"""docstring for TextCNN"""
	def __init__(self, sequence_length, num_classes, vocab_size, embedding_size, filter_size, num_filters, 12_reg_lambda=0.0):
		self.input_x = tf.placeholder(tf.init32, [None, sequence_length], name="input_x")
		self.input_y = tf.placeholder(tf.float32, [None, num_classes], name="input_y")
		self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_porb")

		12_loss = tf.constant(0.0)
		
		super(TextCNN, self).__init__()
		self.arg = arg
		